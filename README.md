# [Frontend - GOV ](https://distracted-nobel-1fde1b.netlify.app/)

Este proyecto representa la vista front inicial de `GOV.CO`, su diseño esta realizado en [Adobe XD](https://xd.adobe.com/spec/db4d4481-94e5-49ad-459a-0b0b0adfaa30-ad7c/) donde se encontrarón todas las funciones de desarrollo para garantizar la correcta maquetación.

Las secciones de diseño implementadas fueron:
* Queremos conocer tu opinión
* Otros temas de interés

Adicionalmente, se realizó la contrucción de un JSON con la información requerida para cada una de las secciones desarrolladas.

## Vista previa

[![Vista previa](https://res.cloudinary.com/dztsfr5dw/image/upload/v1592885013/gov_yo7mbn.png)](https://distracted-nobel-1fde1b.netlify.app/)

## Detalles

* [Angular CLI](https://github.com/angular/angular-cli) version 9.1.9.
* [Bootstrap](https://getbootstrap.com/docs/4.3/getting-started/introduction/) version 4.3.1
* HTML
* CSS

## Uso

Necesitas instalar:
* Git
* Node
* Visual Studio Code

instalar `Angular CLI`:

```
npm install -g @angular/cli
```

## Clonar el respositorio

```
git clone https://gitlab.com/francyrodriguez/frontend-gov.git
```
Lo siguiente es entrar a la carpeta del proyecto e instalar dependencias
```
cd frontend-gov
npm install
```

## Vista previa aplicación

```
ng serve -o
```
Este comando deberá abrir tu navegador predeterminado en `http://localhost:4200/`. La app se recarga automáticamente en caso de modificar el código.

