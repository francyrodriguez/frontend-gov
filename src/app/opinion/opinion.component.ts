import { Opinion, Card } from './../models/model';
import { Component, OnInit } from '@angular/core';
import { sectionOpinion } from '../../providers/data/data.json';

/**
 *
 *
 * @export
 * @class OpinionComponent
 * @implements {OnInit}
 * Sección de Queremos conocer tu opinión
 */
@Component({
  selector: 'app-opinion',
  templateUrl: './opinion.component.html',
  styleUrls: ['./opinion.component.css']
})
export class OpinionComponent implements OnInit {
  /**
   * Declaración de array para tarjeta de opinión
   * y vista de noticias sección "conocer tu opinión"
   */
  public opinions: Array<Opinion> = [];
  public opinionCard: Card;

  constructor() { }

  ngOnInit(): void {
    /**
   * Asignación de la data extraida del archivo "data.json"
   */
    this.opinions = sectionOpinion.opinions;
    this.opinionCard = sectionOpinion.card;
  }


}
