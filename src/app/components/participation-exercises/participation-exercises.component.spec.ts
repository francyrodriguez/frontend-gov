import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ParticipationExercisesComponent } from './participation-exercises.component';

describe('ParticipationExercisesComponent', () => {
  let component: ParticipationExercisesComponent;
  let fixture: ComponentFixture<ParticipationExercisesComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ParticipationExercisesComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ParticipationExercisesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
