import { Opinion } from './../../models/model';
import { Component, OnInit, Input } from '@angular/core';

/**
 *
 *
 * @export
 * @class ParticipationExercisesComponent
 * @implements {OnInit}
 * Vista previa de noticia sobre participación
 */
@Component({
  selector: 'app-participation-exercises',
  templateUrl: './participation-exercises.component.html',
  styleUrls: ['./participation-exercises.component.css']
})
export class ParticipationExercisesComponent implements OnInit {
  @Input() opinion: Opinion;
  @Input() color;

  constructor() { }

  ngOnInit(): void {
    /**
     * *Asignación de color al badge dependiendo del "type" recibido
     */
    switch (this.opinion.type) {
      case "warning":
        this.color = { 'background-color': '#F3561F' }
        break;
      case "success":
        this.color = { 'background-color': '#069169' }
        break;
      case "info":
        this.color = { 'background-color': '#3366CC' }
        break;
      default:
        break;
    }
  }

}
