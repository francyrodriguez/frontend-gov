import { Theme } from './../../models/model';
import { Component, Input } from '@angular/core';

/**
 *
 *
 * @export
 * @class CardInterestComponent
 * Tarjeta informativa de temas de interés
 */
@Component({
  selector: 'app-card-interest',
  templateUrl: './card-interest.component.html',
  styleUrls: ['./card-interest.component.css']
})
export class CardInterestComponent {
  @Input() card: Theme;

}
