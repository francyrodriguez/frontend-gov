import { Component } from '@angular/core';

/**
 *
 *
 * @export
 * @class WrapperComponent
 * Componente encargado de envolver la sección (opinon)
 */
@Component({
  selector: 'app-wrapper',
  templateUrl: './wrapper.component.html',
  styleUrls: ['./wrapper.component.css']
})

export class WrapperComponent {

  constructor() { }

}
