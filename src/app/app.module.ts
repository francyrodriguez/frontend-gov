import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppComponent } from './app.component';
import { HeaderComponent } from './header/header.component';
import { FooterComponent } from './footer/footer.component';
import { OtherThemesComponent } from './other-themes/other-themes.component';
import { OpinionComponent } from './opinion/opinion.component';
import { NavComponent } from './components/nav/nav.component';
import { CardInterestComponent } from './components/card-interest/card-interest.component';
import { WrapperComponent } from './components/wrapper/wrapper.component';
import { ParticipationExercisesComponent } from './components/participation-exercises/participation-exercises.component';

@NgModule({
  declarations: [
    AppComponent,
    HeaderComponent,
    FooterComponent,
    OtherThemesComponent,
    OpinionComponent,
    NavComponent,
    CardInterestComponent,
    WrapperComponent,
    ParticipationExercisesComponent
  ],
  imports: [
    BrowserModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
