import { Theme } from './../models/model';
import { Component, OnInit } from '@angular/core';
import { themes } from '../../providers/data/data.json';

/**
 *
 *
 * @export
 * @class OtherThemesComponent
 * @implements {OnInit}
 * Sección de otros temas de interés
 */
@Component({
  selector: 'app-other-themes',
  templateUrl: './other-themes.component.html',
  styleUrls: ['./other-themes.component.css']
})
export class OtherThemesComponent implements OnInit {
  /**
   * Declaración de array para tarjeta de "temas de interés"
   */
  public cards: Array<Theme> = [];

  constructor() { }

  ngOnInit(): void {
    /**
   * Asignación de la data extraida del archivo "data.json"
   */
    this.cards = themes;
  }

}
