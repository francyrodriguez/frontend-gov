import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { OtherThemesComponent } from './other-themes.component';

describe('OtherThemesComponent', () => {
  let component: OtherThemesComponent;
  let fixture: ComponentFixture<OtherThemesComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ OtherThemesComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(OtherThemesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
