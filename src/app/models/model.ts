/**
 * Modelo de la tarjeta de opinión sección "Conocer opinión"
 */
export interface Card {
  title: string;
  subtitle: string;
  description: string;
}
/**
 * Modelo de la vista previa de noticias sección "Conocer opinión"
 */
export interface Opinion {
  type: string;
  badge: string;
  title: string;
  description: string;
  members: string;
}
/**
 * Modelo de la tarjeta sección "temas de interés"
 */
export interface Theme {
  title: string;
  description: string;
  image: string;
  redirect: Redirect;
}
/**
 * Modelo de redirección de la interface Theme
 */
export interface Redirect {
  path: string;
  name: string;
}
